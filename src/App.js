
import React from 'react';
import CounterClassComp from './Components/CounterClassComp';
import CounterFuncComp from './Components/CounterFuncComp';
import TodoList from './Components/TodoList';
import UseEffectComp from './Components/UseEffectComp';
import CounterTwo from './Components/CounterTwo'
import ReducerCounter from './Components/ReducerCounter';
import DemoUseEffect from './Components/DemoUseEffect';
import AdditionalHooks from './Components/Demo1/AdditionalHooks';

export const AppContext = React.createContext()


function App() {
  return (
    <div>
      <AppContext.Provider value="this is app context">
      <center>
         {/* <CounterClassComp></CounterClassComp>
        <hr/>
        <CounterFuncComp></CounterFuncComp>
        <hr/>
        <TodoList></TodoList>
        <hr></hr> 
        <UseEffectComp></UseEffectComp>
        <hr></hr> */}
        {/* <CounterTwo/> */}
        {/* <ReducerCounter></ReducerCounter> */}
        {/* <DemoUseEffect></DemoUseEffect> */}
        <AdditionalHooks></AdditionalHooks>
        <hr></hr>
        <TodoList></TodoList>
      </center> 
      </AppContext.Provider>
    </div>
    
  );
}

export default App;
