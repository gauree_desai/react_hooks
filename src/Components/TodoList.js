import React,{useState} from 'react'

export default function TodoList  (){
    const [todo, setTodo] = useState("");
    const[todoList,setList]=useState([])
    console.log('Rendering todolist')
    const handleSubmit = (evt) => {
        evt.preventDefault();
        //alert(`Submitting Todo ${todo}`)
        setList([...todoList,todo])
    }


    return (
        <form onSubmit={handleSubmit}>
          
          
            <input
              type="text"
              placeholder="Enter your task here"
              value={todo}
              onChange={e => setTodo(e.target.value)}>

              </input>
           
          <input type="submit" value="Submit" />
          <br/>
          {todoList}
        </form>
      );
}