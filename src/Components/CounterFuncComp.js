import React, { useState } from "react";

export default function CounterFuncComp() {
  const initialCount = 0;
  const [count, setCount] = useState(initialCount);

  const increment = () => {
    setCount((prevcount) => prevcount + 1);
  };

  const decrement = () => {
    setCount((prevcount) => prevcount - 1);
  };

  const reset = () => {
    setCount(0);
  };
  return (
    <div>
      <h1>with hooks</h1>
      <h1>count={count}</h1>
      <button onClick={reset}>RESET</button>
      <button onClick={increment}>Increment By 1</button>
      <button onClick={decrement}>Decrement By 1</button>
    </div>
  );
}
