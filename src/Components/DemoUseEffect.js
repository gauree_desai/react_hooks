import React,{useEffect,useState} from 'react'

export default function DemoUseEffect() {
    const[album,setAlbum]=useState([])

    useEffect(()=>{
        (fetch('https://jsonplaceholder.typicode.com/albums',{method:'GET'})
        .then(response=>response.json())
        .then(data=>setAlbum(data) ))
    },[])
    console.log(album)
    const list= album?.map((item)=>(<li key={item.id}>{item.title}</li>))
       
    return (
         <div>
            {list}
        </div>
    )
}
