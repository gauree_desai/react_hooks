import React, { useCallback, useState } from 'react'
import Button from './Button'
import Display from './Display'
import Title from './Title'

export default function AdditionalHooks() {
    const initialAge=0
    const initialSalary=5000
    const[age,setAge]=useState(initialAge)
    const[salary, setSalary]=useState(initialSalary)


    const incrementAge=()=>{
        setAge(age + 1 )
    }   

    const incrementSalary=()=>{
        setSalary(salary+1000)
    }
    return (
        <div>
           <Title></Title> 
           <Display title='Age' value={age}></Display>
           <Button handleClick={incrementAge}>Increment Age</Button>
           <Display title='Salary' value={salary}></Display>
           <Button handleClick={incrementSalary}>Increment Salary</Button>
        </div>
    )
}
// const incrementAge=useCallback(()=>{
//     setAge(age + 1 )
// },[age]) 