import React from 'react'

 function Button({handleClick,children}) {
    console.log('Button component is rendered-',children)
    return (
        <button onClick={handleClick}>
            {children}
        </button>
        
    )
}
export default Button