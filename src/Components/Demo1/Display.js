import React from 'react'

export default function Display(props) {
    console.log('Display component is rendered-',props.title)
    return (
        <div>
            {props.title }- {props.value}
        </div>
    )
}
