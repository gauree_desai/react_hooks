import React from 'react'

class CounterClassComp extends React.Component{
  constructor(){
      super()
      this.state={
          counter:0
      }
      this.incrementCounter=this.incrementCounter.bind(this)
  }
  incrementCounter(){
      this.setState((prevState)=>( {counter: prevState.counter+1}))
  }
  render(){
      return(
        <div>
            <h1>With Classes</h1>
            <h1>you clicked                                                                                                                                                                                                                                                                                                                                                                                          {this.state.counter} times</h1>
            <button onClick={this.incrementCounter}>Click Me!!!</button>
        </div>
      )
  }
}

export default CounterClassComp