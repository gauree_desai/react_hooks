import React,{useState,useContext} from 'react'
import  { AppContext } from '../App'

export default function UseEffectComp() {
    const [name,setName]=useState('')
    const [age,setAge]=useState(0)
    
    const[formShow, setFormShow]=useState(true)

    React.useEffect(() => {
      // this side effect will run just once, after the first render
      console.log('i act as componentDidMount')
    }, []);
    React.useEffect(() => {
      // this side effect will run only when value1 changes
      console.log('i act as componentDidUpdate only if name changes')
    }, [name]);
    React.useEffect(() => {
      // this side effect will run after every render
      console.log('i run for every render ')
    });
    React.useEffect(() => {
        // this side effect will run after every render
        return ()=>console.log('unmounting....')
      });
  

   const toggle=()=>{
       setFormShow(!formShow)
   }
    
    return (
        <div>
            {/* <form>
                Name:<input name="name"  value={name}
              onChange={e => setName(e.target.value)}></input><br/>
                Age:<input name="age"  value={age}
              onChange={e => setAge(e.target.value)}></input><br/>
            </form> */}
            <button onClick={toggle}>Show/Hide Form</button>
            {formShow &&<FormComp/>}
        </div>
    )
  }

  export function FormComp (){
     const abc= useContext(AppContext)
     console.log('abc:',abc)
      return (
        <div>
            <form>
                Name:<input name="name"  ></input><br/>
                Age:<input name="age"  ></input><br/>
                {abc}
            </form>
        </div>
      )
  }